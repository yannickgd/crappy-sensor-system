/*
 * Copyright (c) 2016-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== adcsinglechannel.c ========
 */
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
#include <math.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/ADC.h>
#include <ti/drivers/ADCBuf.h>
#include <ti/display/Display.h>

/* Example/Board Header files */
#include "Board.h"

/* ADC sample count */
#define ADC_SAMPLE_COUNT  (1024)
#define ADC_REPEAT_COUNT  (200)
#define THREADSTACKSIZE   (1024)

/* ADC conversion result variables */
uint16_t adcValue0;
uint32_t adcValue0MicroVolt;
uint16_t adcValueMIC1[ADC_SAMPLE_COUNT];
uint16_t adcValueMIC2[ADC_SAMPLE_COUNT];
uint16_t adcValueMICdebug[ADC_SAMPLE_COUNT];
uint32_t adcValue1MicroVolt[ADC_SAMPLE_COUNT];
uint16_t dBCounter = 0;
uint16_t dBCounts[ADC_REPEAT_COUNT];

static Display_Handle display;

void adcBufCallback(ADCBuf_Handle handle, ADCBuf_Conversion *conversion,
    void *completedADCBuffer, uint32_t completedChannel)
{
    uint16_t *inputBuffer = (uint16_t *)completedADCBuffer;
    uint16_t k = 0, LV, HV, HighValue, LowValue, dBMeasured, i;
    if(dBCounter < ADC_REPEAT_COUNT){
        //Display_printf(display, 0, 0, "fixing\n");
        for (i = 0; i < 10; i++){
            LV = 0;
            HV = 0;
            HighValue = 0;
            LowValue = 3300;
            for (k = 0; k < ADC_SAMPLE_COUNT; k++) {
                adcValueMICdebug[k] = inputBuffer[k];
                //Display_printf(display, 0, 0, "%d:%d\n", k, inputBuffer[k]);
                if(inputBuffer[k] > HighValue){
                    HighValue = inputBuffer[k];
                    HV = k;
                }
                if(inputBuffer[k] < LowValue){
                    LowValue = inputBuffer[k];
                    LV = k;
                }
            }
            inputBuffer[LV] = 1200;
            inputBuffer[HV] = 1200;
        }

        dBMeasured = HighValue - LowValue;
        if(dBMeasured > 3300){
            dBMeasured = 0;

        }
        dBCounts[dBCounter] = dBMeasured;
        Display_printf(display, 0, 0, "%d:%d\n", dBCounter, dBCounts[dBCounter]);
        dBCounter++;
    }
}

/*
 *  ======== threadFxn0 ========
 *  Open an ADC instance and get a sampling result from a one-shot conversion.
 */
void *threadFxn0(void *arg0)
{
    ADC_Handle   adc;
    ADC_Params   params;
    int_fast16_t res;

    ADC_Params_init(&params);
    adc = ADC_open(Board_ADC7, &params);

    if (adc == NULL) {
        Display_printf(display, 0, 0, "Error initializing ADC channel 0\n");
        while (1);
    }

    /* Blocking mode conversion */
    res = ADC_convert(adc, &adcValue0);

    if (res == ADC_STATUS_SUCCESS) {

        Display_printf(display, 0, 0, "ADC channel 0 raw result: %d\n", adcValue0);
    }
    else {
        Display_printf(display, 0, 0, "ADC channel 0 convert failed\n");
    }

    ADC_close(adc);

    return (NULL);
}

/*
 *  ======== threadFxn1 ========
 *  Open a ADC handle and get an array of sampling results after
 *  calling several conversions.
 */
void *threadFxn1(void *arg0)
{
    uint16_t     i;
    ADCBuf_Handle      adcMIC;
    ADCBuf_Params   ADCMIC_params;
    ADCBuf_init();

    ADCBuf_Conversion continuousConversion;
    continuousConversion.arg = NULL;
    continuousConversion.adcChannel = Board_ADCBUF0CHANNEL0;
    continuousConversion.sampleBuffer = &adcValueMIC1;
    continuousConversion.sampleBufferTwo = &adcValueMIC2;
    continuousConversion.samplesRequestedCount = 1024;
    ADCBuf_Params_init(&ADCMIC_params);
    ADCMIC_params.callbackFxn = adcBufCallback;
    ADCMIC_params.samplingFrequency = 1024;
    ADCMIC_params.returnMode =  ADCBuf_RETURN_MODE_CALLBACK;
    ADCMIC_params.recurrenceMode = ADCBuf_RECURRENCE_MODE_CONTINUOUS;
    adcMIC = ADCBuf_open(Board_ADCBUF0, &ADCMIC_params);
    //adcMIC = ADC_open(Board_ADC0, &ADC_params);
    Display_printf(display, 0, 0, "ADC channel 1 params init\n");
    if (adcMIC == NULL) {
        while (1);//error in opening ADC
    }

    if (ADCBuf_convert(adcMIC, &continuousConversion, 1) !=
        ADCBuf_STATUS_SUCCESS) {
        /* Did not start conversion process correctly. */
        while(1);
    }
    while(dBCounter < ADC_REPEAT_COUNT){
        sleep(1);
    }
    ADCBuf_convertCancel(adcMIC);
    ADCBuf_close(adcMIC);
    for(i = 4; i < ADC_SAMPLE_COUNT; i++){
        Display_printf(display, 0, 0, "%d:%d   %d:%d   %d:%d    %d:%d   %d:%d\n", i-4, adcValueMICdebug[i-4], i-3, adcValueMICdebug[i-3], i-2, adcValueMICdebug[i-2], i-1, adcValueMICdebug[i-1], i, adcValueMICdebug[i]);
        i = i + 4;
    }
    for(i = 0; i < ADC_REPEAT_COUNT; i++){
        Display_printf(display, 0, 0, "Result%d = %d\n", i, dBCounts[i]);
        dBCounts[i] = 0;
    }
    dBCounter = 0;
    return (NULL);
}

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    pthread_t           thread0, thread1;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    /* Call driver init functions */
    ADC_init();
    Display_init();

    /* Open the display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        /* Failed to open display driver */
        while (1);
    }

    Display_printf(display, 0, 0, "Starting the acdsinglechannel example\n");

    /* Create application threads */
    pthread_attr_init(&attrs);

    detachState = PTHREAD_CREATE_DETACHED;
    /* Set priority and stack size attributes */
    retc = pthread_attr_setdetachstate(&attrs, detachState);
    if (retc != 0) {
        /* pthread_attr_setdetachstate() failed */
        while (1);
    }

    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }

    /* Create threadFxn0 thread */
    priParam.sched_priority = 1;
    pthread_attr_setschedparam(&attrs, &priParam);

    retc = pthread_create(&thread0, &attrs, threadFxn0, NULL);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    /* Create threadFxn1 thread */
    retc = pthread_create(&thread1, &attrs, threadFxn1, (void* )0);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    return (NULL);
}
