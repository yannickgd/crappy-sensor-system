/*
 * scss_defines.h
 *
 *  Created on: 25 Jan 2019
 *      Author: yvdongen
 */

#ifndef SCSS_DEFINES_H_
#define SCSS_DEFINES_H_
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/timer/GPTimerCC26XX.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/ADCBuf.h>
#include <ti/drivers/PIN.h>

#define ADC_SAMPLE_COUNT  (1024)
#define ADC_REPEAT_COUNT  (10)
#define ADC_SAMPLE_FREQUENCY (16000)

//merge PMPIN1 with IO21OFF & merge PMPIN2 with IO22OFF
#define PMPIN1      Board_DIO21  | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_DIS
#define PMPIN2      Board_DIO22  | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_DIS
#define PMIO21OFF   Board_DIO21  | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_DIS
#define PMIO21ON    Board_DIO21  | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_BOTHEDGES
#define PMIO22OFF   Board_DIO22  | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_DIS
#define PMIO22ON    Board_DIO22  | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_BOTHEDGES
#define CONFIG5V    Board_DIO11  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX

#define DBREFERENCE 0.22447


struct scss_data {
    /* ADC microphone variables */
//    uint16_t adcValueMIC1[ADC_SAMPLE_COUNT];
//    uint16_t adcValueMIC2[ADC_SAMPLE_COUNT];
//    uint16_t dBCounter;
//    uint16_t dBCounts[ADC_REPEAT_COUNT];

    /* Particulate matter sensor variables */
    PIN_Handle PMHandle;
    PIN_State PMState;
    PIN_State SW5VState;
    GPTimerCC26XX_Handle PM25Timer;
    GPTimerCC26XX_Handle PM10Timer;

    /* Other variables */
    int16_t         calculatedTemperature;
    uint8_t         calculatedHumidity;
    uint32_t        wakeTime;
    uint32_t        heaterTurnOnTime;


    /* Output variables */
    uint16_t        temperature;
    uint16_t        humidity;
    uint16_t        header;
    uint16_t        battery;
    uint16_t        dB;
    uint32_t        time;
    uint32_t        PM10Avg;
    uint32_t        PM25Avg;
    bool            HDC2080OK;
    bool            ADCBatteryOK;
    bool            ADCMicrophoneOK;
    bool            PM10OK;
    bool            PM25OK;
    uint8_t         SF;//0 = SF7, 5 = 12
    uint8_t         SFcounter;

    /* Microphone & battery ADC initialize */
    double          dBDouble;
    double          dBResult;
    uint32_t        dBTemporary;
    uint16_t        adcValueBAT;
    ADC_Handle      adcBAT;
    ADC_Params      ADC_params;
    ADCBuf_Handle   adcMIC;
    ADCBuf_Params   ADCMIC_params;
    ADCBuf_Conversion continuousConversion;
    GPTimerCC26XX_Params params;



    /* RN2483 & UART variables */
    char            input[RN2483_DEFAULT_BUFFER_LENGTH];
    char            output[RN2483_DEFAULT_BUFFER_LENGTH];
    UART_Params     uartParams; //= RN2483_UART_Params_init();
    UART_Handle     uart; //= UART_open(Board_UART0, &uartParams);

    /* I2C start */
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;
    uint8_t         I2CtxBuffer[8];
    uint8_t         I2CrxBuffer[8];
};



#endif /* SCSS_DEFINES_H_ */
