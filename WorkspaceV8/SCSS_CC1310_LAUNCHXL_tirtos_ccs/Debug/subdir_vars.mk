################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC1310_LAUNCHXL_TIRTOS.cmd 

C_SRCS += \
../CC1310_LAUNCHXL.c \
../CC1310_LAUNCHXL_fxns.c \
../HDC2080.c \
../RN2483_UART.c \
../ccfg.c \
../mainThread.c \
../main_tirtos.c \
../scss.c 

C_DEPS += \
./CC1310_LAUNCHXL.d \
./CC1310_LAUNCHXL_fxns.d \
./HDC2080.d \
./RN2483_UART.d \
./ccfg.d \
./mainThread.d \
./main_tirtos.d \
./scss.d 

OBJS += \
./CC1310_LAUNCHXL.obj \
./CC1310_LAUNCHXL_fxns.obj \
./HDC2080.obj \
./RN2483_UART.obj \
./ccfg.obj \
./mainThread.obj \
./main_tirtos.obj \
./scss.obj 

OBJS__QUOTED += \
"CC1310_LAUNCHXL.obj" \
"CC1310_LAUNCHXL_fxns.obj" \
"HDC2080.obj" \
"RN2483_UART.obj" \
"ccfg.obj" \
"mainThread.obj" \
"main_tirtos.obj" \
"scss.obj" 

C_DEPS__QUOTED += \
"CC1310_LAUNCHXL.d" \
"CC1310_LAUNCHXL_fxns.d" \
"HDC2080.d" \
"RN2483_UART.d" \
"ccfg.d" \
"mainThread.d" \
"main_tirtos.d" \
"scss.d" 

C_SRCS__QUOTED += \
"../CC1310_LAUNCHXL.c" \
"../CC1310_LAUNCHXL_fxns.c" \
"../HDC2080.c" \
"../RN2483_UART.c" \
"../ccfg.c" \
"../mainThread.c" \
"../main_tirtos.c" \
"../scss.c" 


