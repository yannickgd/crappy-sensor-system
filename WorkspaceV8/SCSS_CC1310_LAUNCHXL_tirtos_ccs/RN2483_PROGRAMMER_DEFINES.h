/*
 * RN2483_PROGRAMMER_DEFINES.h
 *
 *  Created on: 3 Dec 2018
 *      Author: yvdongen
 */

#ifndef RN2483_PROGRAMMER_DEFINES_H_
#define RN2483_PROGRAMMER_DEFINES_H_
/* SCSS_1 BROWN
#define NWKSKEY         "CA67FAD517B19D306979BF436A207BB0\r\n"
#define APPSKEY         "390A4DC6AE7F0951AEBE1AA304FCF195\r\n"
#define DEVEUI          "007A6C2B70767900\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011F9A\r\n"
*/
///* SCSS_2 RED
#define NWKSKEY         "F20F11BC022049A8411AA468266D1B29\r\n"
#define APPSKEY         "0B924E4030FE79EFC5F061EDB3D17568\r\n"
#define DEVEUI          "00A5564E4DF33E6B\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011A54\r\n"
//*/
/* SCSS_3 GREEN
#define NWKSKEY         "D20CDAB5A19C46EE4951007F04628FAC\r\n"
#define APPSKEY         "B3F8F89F255660197D452B9FD62DEBA3\r\n"
#define DEVEUI          "003C90BA298E8363\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "260117D4\r\n"
*/
/* SCSS_4 WHITE
#define NWKSKEY         "BB9808C69657424E31D282904DE3CA6E\r\n"
#define APPSKEY         "DB28255C4BF3EE10F630171518D891D5\r\n"
#define DEVEUI          "0023BAA8D8F4B573\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011D02\r\n"
*/
/* SCSS_5 YELLOW
#define NWKSKEY         "9D9B0EF01D48F9FED2AE64AACC9F7476\r\n"
#define APPSKEY         "334255CD9F0FEA0DD3EA1475B4495E7A\r\n"
#define DEVEUI          "002C00308574BF5C\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "260117E1\r\n"
*/
/* SCSS_6 BLACK
#define NWKSKEY         "BB2A9CD359054CEF790096FECF29BECE\r\n"
#define APPSKEY         "DDF06B7B0D76C7BEF7DFA5EDAF9F6EDD\r\n"
#define DEVEUI          "002DE0D30C0110BE\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011F93\r\n"
*/
/* SCSS_7 ORANGE
#define NWKSKEY         "C48331867DCA65C17126567225C34EA9\r\n"
#define APPSKEY         "784A84B31A789F31CE14EC1FE438C7E4\r\n"
#define DEVEUI          "00E275070C239FDB\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011B90\r\n"
*/
/* SCSS_8 PURPLE
#define NWKSKEY         "9DF29A120144C4832ED659164A6B90CA\r\n"
#define APPSKEY         "24F9DC86594AA6755EB9FF195931034E\r\n"
#define DEVEUI          "00BB1D5231B0ABAB\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "2601150A\r\n"
*/
/* SCSS_9 GREY
#define NWKSKEY         "BDA9781A1F3482296609E39D3F53AAFE\r\n"
#define APPSKEY         "F48F20485D087C7163CC17CEA1D66E5D\r\n"
#define DEVEUI          "0041312E715CA9FF\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "260115FC\r\n"
*/
/* SCSS_10 BLUE
#define NWKSKEY         "AED950DE80A5EC6D5F7BED22105521E1\r\n"
#define APPSKEY         "327F553F3F53888218C10FFB04CBF5F3\r\n"
#define DEVEUI          "005BE4CFA8275D12\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011F53\r\n"
*/

#define PWRIDX          "1\r\n"
#define SF7DR           "5\r\n"
#define SF8DR           "4\r\n"
#define SF9DR           "3\r\n"
#define SF10DR          "2\r\n"
#define SF11DR          "1\r\n"
#define SF12DR          "0\r\n"

#define SF7             0
#define SF8             1
#define SF9             2
#define SF10            3
#define SF11            4
#define SF12            5

#define ENDMSG          "\r\n"
#define SETNWKSKEY      "mac set nwkskey"
#define SETAPPSKEY      "mac set appskey"
#define SETDEVEUI       "mac set deveui"
#define SETAPPEUI       "mac set appeui"
#define SETDEVADDR      "mac set devaddr"
#define SETPWRIDX       "mac set pwridx"
#define SETDR           "mac set dr"
#define MACSAVE         "mac save \r\n"
#define MACJOIN         "mac join abp\r\n"

#define TESTMSG         "mac tx uncnf 5 AA00FF\r\n"

#define HDC2080OKMSG    "mac tx uncnf 5 FF00FF\r\n"
#define HDC2080NOTOKMSG "mac tx uncnf 5 AA00AA\r\n"
#define TMPHUMDEBUG     "mac tx uncnf 5 %xBB%x\r\n"

#define MICDEBUGMSG     "mac tx uncnf 5 DB%xDB%x\r\n"//total, micLow, micHigh, OldMicLow, OldMicHigh
#define BATDEBUGMSG     "mac tx uncnf 5 B%xB\r\n"

#define OUTPUTMSG       "mac tx uncnf 5 %02x%02x%04x%02x%03x%03x%03x%03x\r\n"//header, battery, time, db, PM10, PM25, temp, hum
#endif /* RN2483_PROGRAMMER_DEFINES_H_ */
