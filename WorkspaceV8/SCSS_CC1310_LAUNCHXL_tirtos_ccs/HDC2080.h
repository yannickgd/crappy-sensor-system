/*
 * HDC2080.h
 *
 *  Created on: 2 Oct 2018
 *      Author: yvdongen
 */

#ifndef HDC2080_H_
#define HDC2080_H_
#include "HDC2080_defines.h"
#include "scss.h"

void HDC2080_check_id(struct scss_data *data);

void HDC2080_single_measure(struct scss_data *data);

void HDC2080_heater_toggle(struct scss_data *data);

uint16_t Concatenate_8_8 (uint8_t MSB, uint8_t LSB);

int16_t Calculate_Temperature(uint16_t TEMP);//returns a 16 bit signed int. Output is 10X temperature. (14.2 degrees outputs as 142)

uint8_t Calculate_Humidity(uint16_t HUMI);


#endif /* HDC2080_H_ */
