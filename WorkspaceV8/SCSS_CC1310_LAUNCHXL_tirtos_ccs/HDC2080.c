/*
 * HDC2080.c
 *
 *  Created on: 2 Oct 2018
 *      Author: yvdongen
 */

#include <unistd.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <stdbool.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <ti/drivers/I2C.h>
//#include <ti/drivers/PIN.h>
#include <ti/drivers/SPI.h>
// #include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"

#include <time.h>
#include <ti/sysbios/hal/Seconds.h>
#include "HDC2080.h"
#include "scss.h"

void HDC2080_check_id(struct scss_data *data){
    if(data->HDC2080OK == false){
        return;
    }
    data->I2CtxBuffer[0] = HDC2080_MANF_ID_LSB_ADDR;
    data->i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
    data->i2cTransaction.writeBuf = data->I2CtxBuffer;
    data->i2cTransaction.writeCount = 1;
    data->i2cTransaction.readBuf = data->I2CrxBuffer;
    data->i2cTransaction.readCount = 4;
    data->HDC2080OK = true;
    if (I2C_transfer(data->i2c, &data->i2cTransaction)) {
       if ((data->I2CrxBuffer[0] != HDC2080_MANF_ID_LSB)|(data->I2CrxBuffer[1] != HDC2080_MANF_ID_MSB)|(data->I2CrxBuffer[2] != HDC2080_DEVC_ID_LSB)|(data->I2CrxBuffer[3] != HDC2080_DEVC_ID_MSB)) {
           data->HDC2080OK = false;
       }
    }
}

void HDC2080_single_measure(struct scss_data *data){
    if(data->HDC2080OK == false){
        return;
    }
    //Start single measurement.
    data->I2CtxBuffer[0] = HDC2080_MEAS_CONF_ADDR;
    data->I2CtxBuffer[1] = HDC2080_TEMP_MEAS_CONF_14b | HDC2080_HUMI_MEAS_CONF_14b | HDC2080_MEAS_CONF_HUMI_TEMP | HDC2080_MEAS_TRIG_START;
    data->i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
    data->i2cTransaction.writeBuf = data->I2CtxBuffer;
    data->i2cTransaction.writeCount = 2;
    data->i2cTransaction.readBuf = data->I2CrxBuffer;
    data->i2cTransaction.readCount = 0;

    if (I2C_transfer(data->i2c, &data->i2cTransaction)) {
    }
    else {
        data->HDC2080OK = false;
    }
    sleep(3);
    //read out single measurement.
    data->I2CtxBuffer[0] = HDC2080_TEMP_MEAS_LSB_ADDR;
    data->i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
    data->i2cTransaction.writeBuf = data->I2CtxBuffer;
    data->i2cTransaction.writeCount = 1;
    data->i2cTransaction.readBuf = data->I2CrxBuffer;
    data->i2cTransaction.readCount = 4;

    if (I2C_transfer(data->i2c, &data->i2cTransaction)) {
        data->temperature = Concatenate_8_8(data->I2CrxBuffer[1], data->I2CrxBuffer[0]);
        data->humidity = Concatenate_8_8(data->I2CrxBuffer[3], data->I2CrxBuffer[2]);
        data->calculatedTemperature = Calculate_Temperature(data->temperature);
        data->calculatedHumidity = Calculate_Humidity(data->humidity);
    }
    else {
        data->HDC2080OK = false;
    }
}

void HDC2080_heater_toggle(struct scss_data *data){
    if(data->HDC2080OK == false){
        return;
    }
    //turn heater on
    data->I2CtxBuffer[0] = HDC2080_RSET_INTR_CONF_ADDR;
    data->I2CtxBuffer[1] = HDC2080_HEAT_ENABLE;
    data->i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
    data->i2cTransaction.writeBuf = data->I2CtxBuffer;
    data->i2cTransaction.writeCount = 2;
    data->i2cTransaction.readBuf = data->I2CrxBuffer;
    data->i2cTransaction.readCount = 0;

    if (I2C_transfer(data->i2c, &data->i2cTransaction)) {
    }
    else {
        data->HDC2080OK = false;
    }
    sleep(5);//leave heater on for 5 seconds
    //turn heater off
    data->I2CtxBuffer[0] = HDC2080_RSET_INTR_CONF_ADDR;
    data->I2CtxBuffer[1] = HDC2080_HEAT_DISABLE;
    data->i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
    data->i2cTransaction.writeBuf = data->I2CtxBuffer;
    data->i2cTransaction.writeCount = 2;
    data->i2cTransaction.readBuf = data->I2CrxBuffer;
    data->i2cTransaction.readCount = 0;

    if (I2C_transfer(data->i2c, &data->i2cTransaction)) {
    }
    else {
        data->HDC2080OK = false;
    }
}

uint16_t Concatenate_8_8 (uint8_t MSB, uint8_t LSB)
{
	return (((uint16_t)MSB << 8) | (uint16_t)LSB);
}

int16_t Calculate_Temperature(uint16_t TEMP)//returns a 16 bit signed int. Output is 10X temperature. (14.2 degrees outputs as 142)
{
	return ((int16_t)(((uint32_t)TEMP * 1650) >> 16) - 400);
}

uint8_t Calculate_Humidity(uint16_t HUMI)
{
	return (uint8_t)(((uint32_t)HUMI * 100) >> 16);
}
