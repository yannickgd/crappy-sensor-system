/*
 * scss.h
 *
 *  Created on: 22 Jan 2019
 *      Author: yvdongen
 */

#ifndef SCSS_H_
#define SCSS_H_

#include "RN2483_UART_DEFINES.h"
#include "HDC2080_defines.h"
#include "scss_defines.h"

void configure_data_struct(struct scss_data *data);

void boot_scss(struct scss_data *data);

void measure_humidity_temperature(struct scss_data *data, uint32_t timeMinutes);

void measure_pm(struct scss_data *data);

void measure_battery(struct scss_data *data);

void open_mic_adc(struct scss_data *data);

void close_mic_adc(struct scss_data *data);

void calculate_db(struct scss_data *data, uint16_t dBCounter, uint16_t dBCounts[ADC_REPEAT_COUNT]);

void switch_sf(struct scss_data *data);

void generate_message(struct scss_data *data);

void send_message(struct scss_data *data);

#endif /* SCSS_H_ */
