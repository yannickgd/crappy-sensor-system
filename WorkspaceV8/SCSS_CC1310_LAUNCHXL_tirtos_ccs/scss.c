/*
 * scss.c
 *
 *  Created on: 25 Jan 2019
 *      Author: yvdongen
 */

#include <unistd.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/timer/GPTimerCC26XX.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/ADCBuf.h>
#include <ti/drivers/PIN.h>

/* Board Header files */
#include "Board.h"

#include <time.h>
#include <ti/sysbios/hal/Seconds.h>
#include "scss.h"
#include "RN2483_UART.h"
#include "HDC2080.h"
#include "RN2483_PROGRAMMER_DEFINES.h"

void configure_data_struct(struct scss_data *data){
    data->calculatedTemperature = 0;
    data->calculatedHumidity = 0;
    data->wakeTime = 0;
    data->heaterTurnOnTime = 0;
    data->temperature = 0;
    data->humidity = 0;
    data->header = 0;
    data->battery = 0;
    data->dB = 0;
    data->time = 0;
    data->PM10Avg = 0;
    data->PM25Avg = 0;
    data->HDC2080OK = true;
    data->ADCBatteryOK = true;
    data->ADCMicrophoneOK = true;
    data->PM10OK = true;
    data->PM25OK = true;
    data->SF = 4;//0 = SF7, 5 = 12
    data->SFcounter = 0;

    data->dBDouble = 0;
    data->dBResult = 0;
    data->dBTemporary = 0;
    data->adcValueBAT = 0;


    ADCBuf_init();
    data->continuousConversion.arg = NULL;
    data->continuousConversion.adcChannel = Board_ADCBUF0CHANNEL0;
//    data->continuousConversion.sampleBuffer = &data->adcValueMIC1;
//    data->continuousConversion.sampleBufferTwo = &data->adcValueMIC2;
    data->continuousConversion.samplesRequestedCount = 1024;
    ADCBuf_Params_init(&data->ADCMIC_params);

    data->ADCMIC_params.samplingFrequency = ADC_SAMPLE_FREQUENCY;
    data->ADCMIC_params.returnMode =  ADCBuf_RETURN_MODE_CALLBACK;
    data->ADCMIC_params.recurrenceMode = ADCBuf_RECURRENCE_MODE_CONTINUOUS;

    const PIN_Config PMPinTable[] = {
        PMPIN1,
        PMPIN2,
        PIN_TERMINATE
    };
    GPTimerCC26XX_Params params;
    GPTimerCC26XX_Params_init(&params);
    data->params = params;
    data->params.width          = GPT_CONFIG_32BIT;
    data->params.mode           = GPT_MODE_ONESHOT_UP;
    data->params.debugStallMode = GPTimerCC26XX_DEBUG_STALL_OFF;
    data->PMHandle = PIN_open(&data->PMState, PMPinTable);

    if(!(data->PMHandle)) {
        while(1);//pm not ok
    }

    PIN_Config SW5VConfig[] = {
        CONFIG5V,
        PIN_TERMINATE
    };

    if(!PIN_open(&data->SW5VState, SW5VConfig)) {// Get handle to this collection of pins
        while(1);//pm not ok
    }
}

void boot_scss(struct scss_data *data){
    GPIO_init();
    I2C_init();
    ADC_init();
    UART_init();
    ADC_Params_init(&data->ADC_params);

    data->uartParams = RN2483_UART_Params_init();
    data->uart = UART_open(Board_UART0, &data->uartParams);
    if (data->uart == NULL) {
        while (1);//failed to open UART
    }

    /* I2C start */
    I2C_Params_init(&data->i2cParams);
    data->i2cParams.bitRate = I2C_400kHz;
    //open i2c handle
    data->i2c = I2C_open(Board_I2C_TMP, &data->i2cParams);
    if (data->i2c == NULL) {
        data->HDC2080OK = false;
    }
}

void measure_humidity_temperature(struct scss_data *data, uint32_t timeMinutes){
    if(data->HDC2080OK == true){
        HDC2080_single_measure(data);
        if((data->calculatedTemperature <= 800) && (data->calculatedHumidity > 50)){//if there is potential for condensation having occurred, and no overheating
            if(data->heaterTurnOnTime < timeMinutes){
                HDC2080_heater_toggle(data);
                data->heaterTurnOnTime = timeMinutes + 180; //wait 3 hours before reheating
            }
        }
    }
}

void measure_pm(struct scss_data *data){
    if(data->PM25Timer == NULL) {
        data->PM25OK = false;
    }
    else{
        data->PM25OK = true;
    }
    if(data->PM10Timer == NULL) {
        data->PM10OK = false;
    }
    else{
        data->PM10OK = true;
    }
    if(data->PM10OK || data->PM25OK){
        PIN_setOutputValue(&data->SW5VState, Board_DIO11, 1);
        sleep(1);
        if(data->PM25OK){
            PIN_setInterrupt(data->PMHandle, PMIO21ON);
            sleep(10);
            PIN_setInterrupt(data->PMHandle, PMIO21OFF);
            GPTimerCC26XX_stop(data->PM25Timer);
            GPTimerCC26XX_close(data->PM25Timer);
        }
        if(data->PM10OK){
            PIN_setInterrupt(data->PMHandle, PMIO22ON);
            sleep(10);
            PIN_setInterrupt(data->PMHandle, PMIO22OFF);
            GPTimerCC26XX_stop(data->PM10Timer);
            GPTimerCC26XX_close(data->PM10Timer);
        }
        PIN_setOutputValue(&data->SW5VState, Board_DIO11, 0);
    }
}

void measure_battery(struct scss_data *data){
    data->adcBAT = ADC_open(Board_ADC7, &data->ADC_params);
    if (data->adcBAT == NULL) {
        data->battery = 0;//error in battery measurement
        data->ADCBatteryOK = false;
    }
    else{
        if (ADC_convert(data->adcBAT, &data->adcValueBAT) == ADC_STATUS_SUCCESS) {
            data->battery = data->adcValueBAT;
            data->battery = data->battery + 100;
            data->battery = (data->battery)/10;
            data->ADCBatteryOK = true;
            if(data->battery > 255){
                data->battery = 255;
                data->ADCBatteryOK = false;
            }
        }
        else {
            data->battery = 0;
            data->ADCBatteryOK = false;
        }
        ADC_close(data->adcBAT);
    }
}

void open_mic_adc(struct scss_data *data){
    data->adcMIC = ADCBuf_open(Board_ADCBUF0, &data->ADCMIC_params);
    data->ADCMicrophoneOK = true;
    if (data->adcMIC == NULL) {
        data->ADCMicrophoneOK = false;
    }

    if (ADCBuf_convert(data->adcMIC, &data->continuousConversion, 1) != ADCBuf_STATUS_SUCCESS) {
        /* Did not start conversion process correctly. */
        data->ADCMicrophoneOK = false;
    }
}

void close_mic_adc(struct scss_data *data){
    ADCBuf_convertCancel(data->adcMIC);
    ADCBuf_close(data->adcMIC);
}

void calculate_db(struct scss_data *data, uint16_t dBCounter, uint16_t dBCounts[ADC_REPEAT_COUNT]){
    data->dBTemporary = 0;
    uint16_t i;
    for(i = 0; i < dBCounter; i++){
        data->dBTemporary = data->dBTemporary + dBCounts[i];
    }
    data->dBTemporary = (data->dBTemporary)/dBCounter;
    data->dBDouble = data->dBTemporary;
    data->dBResult = log10((data->dBDouble)/DBREFERENCE);
    data->dB = (data->dBResult)*20;
    if(data->dB > 255){
        data->dB = 255;
    }
}

void switch_sf(struct scss_data *data){
    if(data->SF == SF11){
        data->SF = SF7;
        data->SFcounter = 0;
    }
    else{
        if(data->SF == SF7){
            if(data->SFcounter == 2){
                data->SF = SF11;
            }
            else{
                data->SFcounter = data->SFcounter + 1;
            }
        }
    }
    RN2483_set_data_rate(data);
}

void generate_message(struct scss_data *data){
    data->header = 0;

    if(data->HDC2080OK){
        data->temperature = data->temperature >> 4;
        data->humidity = data->humidity >> 4;
        data->header += 1;
        data->header += 1 << 1;
    }
    else{
        data->temperature = 0;
        data->humidity = 0;
    }
    if(data->ADCBatteryOK){
    }
    else{
        data->battery = 0;
    }
    if(data->ADCMicrophoneOK){
        data->header += 1 << 4;
    }
    else{
        data->dB = 0;
    }
    if(data->PM10OK){
        data->header += 1 << 3;
    }
    else{
        data->PM10Avg = 0;
    }
    if(data->PM25OK){
        data->header += 1 << 2;
    }
    else{
        data->PM25Avg = 0;
    }
    data->header += data->SF << 5;
    sprintf(data->output, OUTPUTMSG, data->header, data->battery, data->time, data->dB, data->PM10Avg, data->PM25Avg, data->temperature, data->humidity);//header, battery, time, db, PM10, PM25, temp, hum
}

void send_message(struct scss_data *data){
    do{
        UART_write(data->uart, data->output, strlen(data->output));
        RN2483_UART_read(data->uart, data->input);
        sleep(10);
    }while(strcmp(data->input, RN2483_UART_NO_CHANNEL) == 0);
}
