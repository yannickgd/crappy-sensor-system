/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== mainThread.c ========
 */

/* General C header files */
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/timer/GPTimerCC26XX.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/ADCBuf.h>

/* Board Header file */
#include "Board.h"

/* Sensor Library files */
#include "RN2483_UART.h"
#include "RN2483_PROGRAMMER_DEFINES.h"
#include "HDC2080.h"
#include "scss.h"

/* ADC microphone variables */
uint16_t adcValueMIC1[ADC_SAMPLE_COUNT];
uint16_t adcValueMIC2[ADC_SAMPLE_COUNT];
uint16_t dBCounter = 0;
uint16_t dBCounts[ADC_REPEAT_COUNT];

/* ADC callback function */
void adcBufCallback(ADCBuf_Handle handle, ADCBuf_Conversion *conversion,
    void *completedADCBuffer, uint32_t completedChannel)
{
    uint16_t *inputBuffer = (uint16_t *)completedADCBuffer;
    uint16_t k = 0, LV, HV, HighValue, LowValue, dBMeasured, i;
    if(dBCounter < ADC_REPEAT_COUNT){
        for (i = 0; i < 10; i++){
            LV = 0;
            HV = 0;
            HighValue = 0;
            LowValue = 3300;
            for (k = 0; k < ADC_SAMPLE_COUNT; k++) {
                if(inputBuffer[k] > HighValue){
                    HighValue = inputBuffer[k];
                    HV = k;
                }
                if(inputBuffer[k] < LowValue){
                    LowValue = inputBuffer[k];
                    LV = k;
                }
            }
            inputBuffer[LV] = 1200;
            inputBuffer[HV] = 1200;
        }
        dBMeasured = HighValue - LowValue;
        if(dBMeasured > 3300){
            dBMeasured = 0;
        }
        dBCounts[dBCounter] = dBMeasured;
        dBCounter++;
    }
}

/* Particulate matter sensor variables */
GPTimerCC26XX_Handle PM25Timer, PM10Timer;

uint8_t PM25Count = 0, PM10Count = 0;
GPTimerCC26XX_Value PM25Output = 0, PM10Output = 0, PM25Temp = 0, PM10Temp = 0;


/* PM sensor callback */
void PMCallbackFxn(PIN_Handle handle, PIN_Id pinId) {
    volatile uint8_t inputPinState = PIN_getInputValue(pinId);
    switch (pinId) {
        case Board_DIO21:
            if(inputPinState == 1){
                GPTimerCC26XX_start(PM25Timer);
            }
            else{
                GPTimerCC26XX_stop(PM25Timer);
                if(GPTimerCC26XX_getValue(PM25Timer) > (PM25Temp + 96000)){
                    PM25Output += ((GPTimerCC26XX_getValue(PM25Timer) - PM25Temp)/48000)-1;
                    PM25Count++;
                }
                PM25Temp = GPTimerCC26XX_getValue(PM25Timer);
            }
            break;
        case Board_DIO22:
            if(inputPinState == 1){
                GPTimerCC26XX_start(PM10Timer);
            }
            else{
                GPTimerCC26XX_stop(PM10Timer);
                if(GPTimerCC26XX_getValue(PM10Timer) > (PM10Temp + 96000)){
                    PM10Output += ((GPTimerCC26XX_getValue(PM10Timer) - PM10Temp)/48000)-1;
                    PM10Count++;
                }
                PM10Temp = GPTimerCC26XX_getValue(PM10Timer);
            }
            break;
        default:
            break;
    }
}

/* Timer variable */
extern uint32_t timeMinutes;

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    struct scss_data data;
    configure_data_struct(&data);
    data.continuousConversion.sampleBuffer = &adcValueMIC1;
    data.continuousConversion.sampleBufferTwo = &adcValueMIC2;


    if(PIN_registerIntCb(data.PMHandle, &PMCallbackFxn) != 0) {
        data.PM10OK = false;
        data.PM25OK = false;
    }
    data.ADCMIC_params.callbackFxn = adcBufCallback;
    sleep(1);
    boot_scss(&data);
    HDC2080_check_id(&data);//check if HDC2080 is connected
    sleep(1);
    RN2483_join_LORA(&data);
    sleep(10);
    while (1) {
        while(data.wakeTime > timeMinutes){
            sleep(1);
        }
        data.wakeTime = data.wakeTime + 30;

        measure_humidity_temperature(&data, timeMinutes);

        PM25Timer = GPTimerCC26XX_open(CC1310_LAUNCHXL_GPTIMER0A, &data.params);
        data.PM25Timer = PM25Timer;
        PM10Timer = GPTimerCC26XX_open(CC1310_LAUNCHXL_GPTIMER1A, &data.params);
        data.PM10Timer = PM10Timer;
        measure_pm(&data);
        data.PM25Avg = PM25Output/PM25Count;
        PM25Output = 0;
        PM25Count = 0;
        data.PM10Avg = PM10Output/PM10Count;
        PM10Output = 0;
        PM10Count = 0;
        sleep(1);

        measure_battery(&data);
        sleep(1);

        open_mic_adc(&data);
        while(dBCounter < ADC_REPEAT_COUNT){
            sleep(1);
        }
        close_mic_adc(&data);
        calculate_db(&data, dBCounter, dBCounts);
        dBCounter = 0;

        data.time = timeMinutes;
        sleep(1);
        generate_message(&data);
        sleep(1);
        send_message(&data);
        sleep(1);
        switch_sf(&data);
    }
}
