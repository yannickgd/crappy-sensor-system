/*
 * RN2483_UART.h
 *
 *  Created on: 16 Oct 2018
 *      Author: yvdongen
 */

#ifndef RN2483_UART_H_
#define RN2483_UART_H_


#include "RN2483_UART_DEFINES.h"
#include "scss.h"

UART_Params RN2483_UART_Params_init();
void RN2483_UART_read(UART_Handle handle, void *buffer);
void RN2483_join_LORA(struct scss_data *data);
void RN2483_set_data_rate(struct scss_data *data);
void RN2483_string_split_2(char *out1, char *out2, char *input);
void RN2483_string_split_3(char *out1, char *out2, char *out3, char *input);
void RN2483_string_merge_2(char *out, char *input1, char *input2);
void RN2483_string_merge_3(char *out, char *input1, char *input2, char *input3);
void RN2483_string_to_decimal(uint32_t *out, char *in);
void RN2483_string_to_hexadecimal(uint32_t *out, char *in);





#endif /* RN2483_UART_H_ */
