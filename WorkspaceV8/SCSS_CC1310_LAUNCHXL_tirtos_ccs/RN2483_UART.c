/*
 * RN2483_UART.c
 *
 *  Created on: 16 Oct 2018
 *      Author: yvdongen
 */
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

#include "scss.h"
#include "RN2483_UART.h"
#include "RN2483_PROGRAMMER_DEFINES.h"


UART_Params RN2483_UART_Params_init(){
    UART_Params params;
    UART_Params_init(&params);
    params.writeDataMode = UART_DATA_BINARY;
    params.readDataMode = UART_DATA_BINARY;
    params.readReturnMode = UART_RETURN_FULL;
    params.readEcho = UART_ECHO_OFF;
    params.baudRate = RN2483_UART_BAUD_RATE;
    //params.readTimeout = RN2483_UART_TIME_OUT;
    return params;
}

void RN2483_UART_read(UART_Handle handle, void *buffer){
    uint8_t i = 0;
    uint8_t *currentAddress = buffer;
    //char debug[256];
    for(i = 0; i < RN2483_DEFAULT_BUFFER_LENGTH; i++){
        UART_read(handle, currentAddress, 1);
        //sprintf(debug, "\nInput was DEC%i\n", *currentAddress);
        //UART_write(handle, debug, strlen(debug));
        if(*currentAddress == 0x0D | *currentAddress == 0x0A){
            *currentAddress = NULL;
            return;
        }
        else{
            currentAddress++;
        }
    }
}

void RN2483_join_LORA(struct scss_data *data){

    RN2483_string_merge_2(data->output, SETNWKSKEY, NWKSKEY);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    RN2483_string_merge_2(data->output, SETAPPSKEY, APPSKEY);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    RN2483_string_merge_2(data->output, SETDEVEUI, DEVEUI);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    RN2483_string_merge_2(data->output, SETAPPEUI, APPEUI);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    RN2483_string_merge_2(data->output, SETDEVADDR, DEVADDR);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    RN2483_string_merge_2(data->output, SETPWRIDX, PWRIDX);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    RN2483_string_merge_2(data->output, SETDR, SF11DR);
    UART_write(data->uart, data->output, strlen(data->output));
    sleep(1);
    UART_write(data->uart, MACSAVE, strlen(MACSAVE));
    sleep(1);
    RN2483_string_merge_2(data->output, MACJOIN, ENDMSG);
    UART_write(data->uart, MACJOIN, strlen(MACJOIN));
}

void RN2483_set_data_rate(struct scss_data *data){
    switch(data->SF){
    case SF7:
        RN2483_string_merge_2(data->output, SETDR, SF7DR);
        break;
    case SF8:
        RN2483_string_merge_2(data->output, SETDR, SF8DR);
        break;
    case SF9:
        RN2483_string_merge_2(data->output, SETDR, SF9DR);
        break;
    case SF10:
        RN2483_string_merge_2(data->output, SETDR, SF10DR);
        break;
    case SF11:
        RN2483_string_merge_2(data->output, SETDR, SF11DR);
        break;
    case SF12:
        RN2483_string_merge_2(data->output, SETDR, SF12DR);
        break;
    default:
        RN2483_string_merge_2(data->output, SETDR, SF7DR);
        break;
    }
    UART_write(data->uart, data->output, strlen(data->output));
}

void RN2483_string_split_2(char *out1, char *out2, char *input){
	strcpy(out1, input);
	char *temp;
	temp = strchr(out1, ' ');
	if(temp != NULL){
	    *temp = NULL;
	    strcpy(out2, (temp+1));
	}
}

void RN2483_string_split_3(char *out1, char *out2, char *out3, char *input) {
	RN2483_string_split_2(out1, out2, input);
	RN2483_string_split_2(out2, out3, out2);
}

void RN2483_string_merge_2(char *out, char *input1, char *input2){
	strcpy(out, input1);
	strcat(out, " ");
	strcat(out, input2);
}

void RN2483_string_merge_3(char *out, char *input1, char *input2, char *input3){
	RN2483_string_merge_2(out, input1, input2);
	RN2483_string_merge_2(out, out, input3);
}

void RN2483_string_to_decimal(uint32_t *out, char *in){
	char *endptr;
	*out = strtoumax(in, &endptr, 10);
}

void RN2483_string_to_hexadecimal(uint32_t *out, char *in){
	char *endptr;
	*out = strtoumax(in, &endptr, 16);
}
