/*
 * RN2483_UART_DEFINES.h
 *
 *  Created on: 16 Oct 2018
 *      Author: yvdongen
 */

#ifndef RN2483_UART_DEFINES_H_
#define RN2483_UART_DEFINES_H_
#define RN2483_DEFAULT_BUFFER_LENGTH    128
#define RN2483_UART_BAUD_RATE		    57600
#define RN2483_UART_TIME_OUT		    1000000

#define RN2483_UART_CONNECT         "mac join abp\r\n"
#define RN2483_UART_SEND            "mac tx uncnf %i 0f%i\r\n"
#define RN2483_UART_NO_CHANNEL      "no_free_ch"

#endif /* RN2483_UART_DEFINES_H_ */
