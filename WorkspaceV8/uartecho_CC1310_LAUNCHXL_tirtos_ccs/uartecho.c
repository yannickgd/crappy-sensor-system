/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== uartecho.c ========
 */
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Example/Board Header files */
#include "Board.h"

#include "RN2483_UART.h"
/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    char        input[RN2483_DEFAULT_BUFFER_LENGTH];
    //char        debug[RN2483_DEFAULT_BUFFER_LENGTH];
    char        output[RN2483_DEFAULT_BUFFER_LENGTH];



    /* Call driver init functions */
    GPIO_init();
    UART_init();

    /* Configure the LED pin */
    //GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    /* Turn on user LED */
    //GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);

    /* Create a UART with data processing off. */
    UART_Params uartParams = RN2483_UART_Params_init();
    UART_Handle uart = UART_open(Board_UART0, &uartParams);
    if (uart == NULL) {
        /* UART_open() failed */
        while (1);
    }
    sleep(1);
    UART_write(uart, RN2483_UART_CONNECT, sizeof(RN2483_UART_CONNECT));
    sleep(1);
    uint16_t i = 0, j = 1;
    while(1){
        sprintf(output, RN2483_UART_SEND, j, i);
        UART_write(uart, output, strlen(output));
        RN2483_UART_read(uart, input);
        if(strcmp(input, RN2483_UART_NO_CHANNEL) == 0){
            sleep(10);
        }
        else{
            i++;
        }

        sleep(10);
    }

    //UART_write(uart, echoPrompt, sizeof(echoPrompt));
    //UART_write(uart, debugMSG, sizeof(debugMSG));
    //RN2483_UART_read(uart, &input);
    //uint16_t length = strlen(input);
    /*uint16_t i = 0;
    for(i = 0; i < length; i++){
        if(input[i] == 0x0D){
            compare[i] = NULL;
            break;
        }
        else{
            compare[i] = input[i];
        }
    }*/


    //sprintf(debug, inputVSWanted, length);
    //UART_write(uart, debug, strlen(debug));
    /*length = strlen(compare);
    sprintf(debug, "Compare was %i characters long\n", length);
    UART_write(uart, debug, strlen(debug));*/
    /*if(strcmp(wantedEcho, input) == 0){
        UART_write(uart, validResponse, sizeof(validResponse));
    }
    else{
        UART_write(uart, invalidResponse, sizeof(invalidResponse));
        uint16_t i = 0;
        for(i = 0; i < strlen(wantedEcho); i++){
            sprintf(debug, WANTEDCHARGOTTENCHAR, wantedEcho[i], input[i]);
            UART_write(uart, debug, strlen(debug));
        }

        //UART_write(uart, input, strlen(input));

        //UART_write(uart, input[0], 1);
    }*/
//    char        split1[RN2483_DEFAULT_BUFFER_LENGTH];
//    char        split2[RN2483_DEFAULT_BUFFER_LENGTH];
//    char        split3[RN2483_DEFAULT_BUFFER_LENGTH];
//    UART_write(uart, threeWordPrompt, strlen(threeWordPrompt));
//    RN2483_UART_read(uart, &input);
//    RN2483_string_split_3(split1, split2, split3, input);
//    sprintf(debug, ABCMSG, split1, split2, split3);
//    UART_write(uart, debug, strlen(debug));
//    RN2483_string_merge_3(debug, split1, split2, split3);
//    strcpy(split1, debug);
//    sprintf(debug, THREESTRINGMSG, split1);
//    UART_write(uart, debug, strlen(debug));


    /* Loop forever echoing */
   // while (1) {
            //UART_write(uart, &input, 1);
    //}
}
