################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC1310_LAUNCHXL_TIRTOS.cmd 

C_SRCS += \
../CC1310_LAUNCHXL.c \
../CC1310_LAUNCHXL_fxns.c \
../RN2483_UART.c \
../ccfg.c \
../main_tirtos.c \
../uartecho.c 

C_DEPS += \
./CC1310_LAUNCHXL.d \
./CC1310_LAUNCHXL_fxns.d \
./RN2483_UART.d \
./ccfg.d \
./main_tirtos.d \
./uartecho.d 

OBJS += \
./CC1310_LAUNCHXL.obj \
./CC1310_LAUNCHXL_fxns.obj \
./RN2483_UART.obj \
./ccfg.obj \
./main_tirtos.obj \
./uartecho.obj 

OBJS__QUOTED += \
"CC1310_LAUNCHXL.obj" \
"CC1310_LAUNCHXL_fxns.obj" \
"RN2483_UART.obj" \
"ccfg.obj" \
"main_tirtos.obj" \
"uartecho.obj" 

C_DEPS__QUOTED += \
"CC1310_LAUNCHXL.d" \
"CC1310_LAUNCHXL_fxns.d" \
"RN2483_UART.d" \
"ccfg.d" \
"main_tirtos.d" \
"uartecho.d" 

C_SRCS__QUOTED += \
"../CC1310_LAUNCHXL.c" \
"../CC1310_LAUNCHXL_fxns.c" \
"../RN2483_UART.c" \
"../ccfg.c" \
"../main_tirtos.c" \
"../uartecho.c" 


