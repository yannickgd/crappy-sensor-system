/*
 * RN2483_UART_DEFINES.h
 *
 *  Created on: 16 Oct 2018
 *      Author: yvdongen
 */

#ifndef RN2483_UART_DEFINES_H_
#define RN2483_UART_DEFINES_H_
#define RN2483_DEFAULT_BUFFER_LENGTH    128
#define RN2483_UART_BAUD_RATE		    57600
#define RN2483_UART_TIME_OUT		    1000000

#define RN2483_UART_CONNECT         "mac join abp\r\n"
#define RN2483_UART_SEND            "mac tx uncnf %i 0f%i\r\n"
#define RN2483_UART_NO_CHANNEL      "no_free_ch"

/* temporary debug messages for testing*/
#define debugMSG                    "this is a debug message\r\n"
#define echoPrompt                  "please type response\r\n"//"sys get ver\r\n";
#define threeWordPrompt             "please type 3 words\r\n"
#define validResponse               "Valid response\r\n"
#define invalidResponse             "Invalid response\r\n"
#define wantedEcho                  "response"
#define inputVSWanted               "Input was %i characters long, wanted 8\n"
#define ABCMSG                      "\nA: %s B: %s C: %s\n"
#define THREESTRINGMSG              "\nthree strings were: %s\r\n"
#define WANTEDCHARGOTTENCHAR        "\nWanted %c got %c\n"


#endif /* RN2483_UART_DEFINES_H_ */
