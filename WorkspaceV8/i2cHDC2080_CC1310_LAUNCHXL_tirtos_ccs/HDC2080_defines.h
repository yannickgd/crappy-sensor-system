/*
 * HDC2080_defines.h
 *
 *  Created on: 2 Oct 2018
 *      Author: yvdongen
 */

#ifndef HDC2080_DEFINES_H_
#define HDC2080_DEFINES_H_

//#define HDC2080_ADDR_READ_LOW								0x40//Address used if ADDR pin is connected to GND
//#define HDC2080_ADDR_READ_HIGH							0x41//Address used if ADDR pin is connected to VCC
//#define HDC2080_ADDR_WRITE_LOW							0xC0//Address used if ADDR pin is connected to GND
//#define HDC2080_ADDR_WRITE_HIGH							0xC1//Address used if ADDR pin is connected to VCC
#define HDC2080_ADDR_LOW								0x40//Address used if ADDR pin is connected to GND
#define HDC2080_ADDR_HIGH								0x41//Address used if ADDR pin is connected to VCC
#define HDC2080_TEMP_MEAS_LSB_ADDR						0x00//Measured Temperature bits 7:0 address
#define HDC2080_TEMP_MEAS_MSB_ADDR						0x01//Measured Temperature bits 15:8 address
#define HDC2080_HUMI_MEAS_LSB_ADDR						0x02//Measured Humidity bits 7:0 address
#define HDC2080_HUMI_MEAS_MSB_ADDR						0x03//Measured Humidity bits 15:8 address
#define HDC2080_INTR_DRDY_STAT_ADDR						0x04//DataReady interrupt status address
#define HDC2080_TEMP_MEAS_MAX_ADDR						0x05//Maximum measured temperature address
#define HDC2080_HUMI_MEAS_MAX_ADDR						0x06//Maximum measured humidity address
#define HDC2080_INTR_ENABLE_ADDR						0x07//Interrupt enable address
#define HDC2080_TEMP_OFST_ADJ_ADDR						0x08//Temperature offset adjustment address
#define HDC2080_HUMI_OFST_ADJ_ADDR						0x09//Humidity offset adjustment address
#define HDC2080_TEMP_THR_LOW_ADDR						0x0A//Temperature treshold low address
#define HDC2080_TEMP_THR_HIGH_ADDR						0x0B//Temperature treshold high address
#define HDC2080_HUMI_THR_LOW_ADDR						0x0C//Humidity treshold low address
#define HDC2080_HUMI_THR_HIGH_ADDR						0x0D//Humidity treshold high address
#define HDC2080_RSET_INTR_CONF_ADDR						0x0E//Soft reset and interrupt configuration address
#define HDC2080_MEAS_CONF_ADDR							0x0F//Measurement configuration address
#define HDC2080_MANF_ID_LSB_ADDR						0xFC//Manufacturer ID LSB address
#define HDC2080_MANF_ID_MSB_ADDR						0xFD//Manufacturer ID MSB address
#define HDC2080_DEVC_ID_LSB_ADDR						0xFE//Device ID LSB address
#define HDC2080_DEVC_ID_MSB_ADDR						0xFF//Device ID MSB address

#define HDC2080_MANF_ID_LSB								0x49//Manufacturer ID lSB
#define HDC2080_MANF_ID_MSB								0x54//Manufacturer ID MSB
#define HDC2080_DEVC_ID_LSB								0xD0//Device ID LSB
#define HDC2080_DEVC_ID_MSB								0x07//Device ID MSB

#define HDC2080_DRDY_INTR								0x80//Data ready bit
#define HDC2080_TEMP_THR_LOW_INTR						0x40//Temperature threshold low interrupt
#define HDC2080_TEMP_THR_HIGH_INTR						0x20//Temperature threshold high interrupt
#define HDC2080_HUMI_THR_LOW_INTR						0x10//Humidity threshold low interrupt
#define HDC2080_HUMI_THR_HIGH_INTR						0x08//Humidity threshold high interrupt
#define HDC2080_DRDY_MASK								0xF8//Mask for data ready and interrupt config register (0x04 & 0x07)
#define HDC2080_TEMP_MEAS_CONF_14b						0x00//Set temperature measurement resolution to 14 bits
#define HDC2080_TEMP_MEAS_CONF_11b						0x40//Set temperature measurement resolution to 11 bits
#define HDC2080_TEMP_MEAS_CONF_8b						0x80//Set temperature measurement resolution to 8 bits
#define HDC2080_HUMI_MEAS_CONF_14b						0x00//Set humidity measurement resolution to 14 bits
#define HDC2080_HUMI_MEAS_CONF_11b						0x10//Set humidity measurement resolution to 11 bits
#define HDC2080_HUMI_MEAS_CONF_8b						0x20//Set humidity measurement resolution to 8 bits
#define HDC2080_MEAS_CONF_MASK							0xF7//Mask for reading/writing Measurement configuration (0x0F)
#define HDC2080_MEAS_CONF_HUMI_TEMP						0x00//Set sensor to measure humidity and temperature
#define HDC2080_MEAS_CONF_HUMI_ONLY						0x04//Set sensor to measure humidity only
#define HDC2080_MEAS_CONF_TEMP_ONLY						0x02//Set sensor to measure temperature only
#define HDC2080_MEAS_TRIG_START							0x01//Starts a single measurement, bit self clears
#define HDC2080_SOFT_RES_START							0x80//Soft resets the sensor
#define HDC2080_AMM_DISABLE								0x00//Disables auto measurement
#define HDC2080_AMM_1_120_HZ							0x10//Sets auto measurement to 1/120Hz
#define HDC2080_AMM_1_60_HZ								0x20//Sets auto measurement to 1/60Hz
#define HDC2080_AMM_1_10_HZ								0x30//Sets auto measurement to 1/10Hz
#define HDC2080_AMM_2_10_HZ								0x40//Sets auto measurement to 2/10Hz
#define HDC2080_AMM_1_HZ								0x50//Sets auto measurement to 1Hz
#define HDC2080_AMM_2_HZ								0x60//Sets auto measurement to 2Hz
#define HDC2080_AMM_5_HZ								0x70//Sets auto measurement to 5Hz
#define HDC2080_HEAT_ENABLE								0x08//Enables internal heater
#define HDC2080_HEAT_DISABLE							0x00//Disables internal heater
#define HDC2080_DRDY_PIN_ENABLE							0x04//Data ready pin enabled
#define HDC2080_DRDY_PIN_DISABLE						0x00//Data ready pin High Z
#define HDC2080_INTR_POL_LOW							0x00//Interrupt polarity active low
#define HDC2080_INTR_POL_HIGH							0x02//Interrupt polarity active high
#define HDC2080_INTR_MODE_LOW							0x00//Interrupt level sensitive
#define HDC2080_INTR_MODE_HIGH							0x01//Interrupt comperator mode

#endif /* HDC2080_DEFINES_H_ */
