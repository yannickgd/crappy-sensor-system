/*
 * Copyright (c) 2016-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *    ======== i2ctmp007.c ========
 */
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/display/Display.h>

/* Example/Board Header files */
#include "Board.h"
#include "HDC2080.h"

#define TASKSTACKSIZE       640

#define TMP007_DIE_TEMP     0x0001  /* Die Temp Result Register */
#define TMP007_OBJ_TEMP     0x0003  /* Object Temp Result Register */

static Display_Handle display;

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    unsigned int    i;
    int16_t			temperature;
	uint8_t			humidity;
    uint8_t         txBuffer[8];
    uint8_t         rxBuffer[8];
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

    /* Call driver init functions */
    Display_init();
    GPIO_init();
    I2C_init();

    /* Configure the LED pin */
    GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    /* Open the HOST display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        while (1);
    }

    /* Turn on user LED */
    GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);
    Display_printf(display, 0, 0, "Starting the i2cHDC2080 example\n");

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(Board_I2C_TMP, &i2cParams);
    if (i2c == NULL) {
        Display_printf(display, 0, 0, "Error Initializing I2C\n");
        while (1);
    }
    else {
        Display_printf(display, 0, 0, "I2C Initialized!\n");
    }

    /* Point to the T ambient register and read its 2 bytes */



	sleep(1);
	//Start single measurement.
	txBuffer[0] = HDC2080_MEAS_CONF_ADDR;
	txBuffer[1] = HDC2080_TEMP_MEAS_CONF_14b | HDC2080_HUMI_MEAS_CONF_14b | HDC2080_MEAS_CONF_HUMI_TEMP | HDC2080_MEAS_TRIG_START;
	i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
	i2cTransaction.writeBuf = txBuffer;
	i2cTransaction.writeCount = 2;
	i2cTransaction.readBuf = rxBuffer;
	i2cTransaction.readCount = 0;

	if (I2C_transfer(i2c, &i2cTransaction)) {
		/* Extract degrees C from the received data; see TMP102 datasheet */
		Display_printf(display, 0, 0, "measurement started\n");
	}
	else {
		Display_printf(display, 0, 0, "I2C Bus fault\n");
	}
	//Start single measurement.
	txBuffer[0] = HDC2080_TEMP_MEAS_LSB_ADDR;
	i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
	i2cTransaction.writeBuf = txBuffer;
	i2cTransaction.writeCount = 1;
	i2cTransaction.readBuf = rxBuffer;
	i2cTransaction.readCount = 4;

	if (I2C_transfer(i2c, &i2cTransaction)) {
		/* Extract degrees C from the received data; see TMP102 datasheet */
		temperature = Calculate_Temperature(Concatenate_8_8(rxBuffer[1], rxBuffer[0]));
		humidity = Calculate_Humidity(Concatenate_8_8(rxBuffer[3], rxBuffer[2]));
		Display_printf(display, 0, 0, "Temperature %d (C), Humidity %d (RH)\n BIN DATA %d,  %d, %d, %d\n", temperature, humidity, rxBuffer[0], rxBuffer[1], rxBuffer[2], rxBuffer[3]);
	}
	else {
		Display_printf(display, 0, 0, "I2C Bus fault\n");
	}
	sleep(1);
	txBuffer[0] = HDC2080_MANF_ID_LSB_ADDR;
    i2cTransaction.slaveAddress = HDC2080_ADDR_LOW;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = rxBuffer;
    i2cTransaction.readCount = 4;

    if (I2C_transfer(i2c, &i2cTransaction)) {
       /* Extract degrees C from the received data; see TMP102 datasheet */
       if ((rxBuffer[0] != HDC2080_MANF_ID_LSB)|(rxBuffer[1] != HDC2080_MANF_ID_MSB)|(rxBuffer[2] != HDC2080_DEVC_ID_LSB)|(rxBuffer[3] != HDC2080_DEVC_ID_MSB)) {
        Display_printf(display, 0, 0, "INVALID DEVICE\n");
       }
       else {
           Display_printf(display, 0, 0, "VALID DEVICE, %d\n", rxBuffer[0]);
       }
    }
    else {
        Display_printf(display, 0, 0, "I2C Bus fault\n");
    }
    /* Take 20 samples and print them out onto the console */
//    for (i = 0; i < 20; i++) {
//        if (I2C_transfer(i2c, &i2cTransaction)) {
//            /* Extract degrees C from the received data; see TMP102 datasheet */
//            temperature = (rxBuffer[0] << 6) | (rxBuffer[1] >> 2);
//
//            /*
//             * If the MSB is set '1', then we have a 2's complement
//             * negative value which needs to be sign extended
//             */
//            if (rxBuffer[0] & 0x80) {
//                temperature |= 0xF000;
//            }
//           /*
//            * For simplicity, divide the temperature value by 32 to get rid of
//            * the decimal precision; see TI's TMP007 datasheet
//            */
//            temperature /= 32;
//
//            Display_printf(display, 0, 0, "Sample %u: %d (C)\n", i, temperature);
//        }
//        else {
//            Display_printf(display, 0, 0, "I2C Bus fault\n");
//        }
//
//        /* Sleep for 1 second */
//        sleep(1);
//    }

    /* Deinitialized I2C */
    I2C_close(i2c);
    Display_printf(display, 0, 0, "I2C closed!\n");

    return (NULL);
}
