/*
 * HDC2080.c
 *
 *  Created on: 2 Oct 2018
 *      Author: yvdongen
 */

#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <ti/drivers/I2C.h>
//#include <ti/drivers/PIN.h>
#include <ti/drivers/SPI.h>
// #include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header files */
#include "Board.h"

#include <time.h>
#include <ti/sysbios/hal/Seconds.h>
#include "HDC2080.h"

uint16_t Concatenate_8_8 (uint8_t MSB, uint8_t LSB)
{
	return (((uint16_t)MSB << 8) | (uint16_t)LSB);
}

int16_t Calculate_Temperature(uint16_t TEMP)//returns a 16 bit signed int. Output is 10X temperature. (14.2 degrees outputs as 142)
{
	return ((int16_t)(((uint32_t)TEMP * 1650) >> 16) - 400);
}

uint8_t Calculate_Humidity(uint16_t HUMI)
{
	return (uint8_t)(((uint32_t)HUMI * 100) >> 16);
}
