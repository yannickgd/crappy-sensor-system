/*
 * RN2483_PROGRAMMER_DEFINES.h
 *
 *  Created on: 3 Dec 2018
 *      Author: yvdongen
 */

#ifndef RN2483_PROGRAMMER_DEFINES_H_
#define RN2483_PROGRAMMER_DEFINES_H_

#define NWKSKEY         "F20F11BC022049A8411AA468266D1B29\r\n"
#define APPSKEY         "0B924E4030FE79EFC5F061EDB3D17568\r\n"
#define DEVEUI          "00A5564E4DF33E6B\r\n"
#define APPEUI          "70B3D57ED0013716\r\n"
#define DEVADDR         "26011A54\r\n"
#define PWRIDX          "1\r\n"
#define DR              "0\r\n"

#define ENDMSG          "\r\n"
#define SETNWKSKEY      "mac set nwkskey"
#define SETAPPSKEY      "mac set appskey"
#define SETDEVEUI       "mac set deveui"
#define SETAPPEUI       "mac set appeui"
#define SETDEVADDR      "mac set devaddr"
#define SETPWRIDX       "mac set pwridx"
#define SETDR           "mac set dr"
#define MACSAVE         "mac save \r\n"
#define MACJOIN         "mac join abp\r\n"

#define TESTMSG         "mac tx uncnf 5 AA00FF\r\n"

#endif /* RN2483_PROGRAMMER_DEFINES_H_ */
