/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== mainThread.c ========
 */

/* For usleep() */
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
// #include <ti/drivers/SDSPI.h>
// #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>

/* Board Header file */
#include "Board.h"
#include "RN2483_UART.h"
#include "RN2483_PROGRAMMER_DEFINES.h"






/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    char        input[RN2483_DEFAULT_BUFFER_LENGTH];
    char        output[RN2483_DEFAULT_BUFFER_LENGTH];
    //Types_FreqHz  freq;
    //BIOS_getCpuFreq(&freq);
    //GPTimerCC26XX_Value loadVal = 47999;
    //GPTimerCC26XX_setLoadValue(PM25Timer, loadVal);
    //GPTimerCC26XX_registerInterrupt(hTimer, timerCallback, GPT_INT_TIMEOUT);

    /* Call driver init functions */
    GPIO_init();
    // I2C_init();
    // SDSPI_init();
    // SPI_init();
    UART_init();
    // Watchdog_init();
    //printf("startup\n");
    //GPTimerCC26XX_start(PM25Timer);

    UART_Params uartParams = RN2483_UART_Params_init();
    UART_Handle uart = UART_open(Board_UART0, &uartParams);
    if (uart == NULL) {
        /* UART_open() failed */
        while (1);
    }
    sleep(1);
    RN2483_string_merge_2(output, SETNWKSKEY, NWKSKEY);
    UART_write(uart, output, strlen(output));
    sleep(1);
    RN2483_string_merge_2(output, SETAPPSKEY, APPSKEY);
    UART_write(uart, output, strlen(output));
    sleep(1);
    RN2483_string_merge_2(output, SETDEVEUI, DEVEUI);
    UART_write(uart, output, strlen(output));
    sleep(1);
    RN2483_string_merge_2(output, SETAPPEUI, APPEUI);
    UART_write(uart, output, strlen(output));
    sleep(1);
    RN2483_string_merge_2(output, SETDEVADDR, DEVADDR);
    UART_write(uart, output, strlen(output));
    sleep(1);
    RN2483_string_merge_2(output, SETPWRIDX, PWRIDX);
    UART_write(uart, output, strlen(output));
    sleep(1);
    RN2483_string_merge_2(output, SETDR, DR);
    UART_write(uart, output, strlen(output));
    sleep(1);
    UART_write(uart, MACSAVE, strlen(MACSAVE));
    sleep(1);
    RN2483_string_merge_2(output, MACJOIN, ENDMSG);
    UART_write(uart, MACJOIN, strlen(MACJOIN));
    sleep(1);
    while (1) {
        UART_write(uart, TESTMSG, strlen(TESTMSG));
        sleep(30);
    }
}
